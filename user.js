var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var mailer = require('nodemailer');
var excel = require("excel4node");

const pool = mysql.createPool({
    host: 'localhost',
    user: 'usr',
    password: 'usr',
    database: 'test_db',
    connectionLimit: 10,
    multipleStatements: true,
});

router.get('/', function(req, res){
    let user = req.query;
    let sql = `
        select id, code, name
          from user
         where code like('%${user.code}%')
           and name like('%${user.name}%')
    `
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    res.end("sql error");
                }else {
                    res.json(result);
                }
            });
        }
    });
});

router.post('/', function(req, res){
    let user = req.body;
    let sql = `
        insert into user( code, name, pwd)
        values('${user.code}', '${user.name}', '${user.pwd}' )
    `;
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    res.end("sql error");
                }else {
                    config = 
                    {
                        "host": "smtp.gmail.com",
                        "port": 465,
                        "secure": true,
                        "auth": {
                            "user": "www.saleagent@gmail.com",
                            "pass": "www.saleagent1234"
                        }
                    };
                    let smtp = mailer.createTransport(config);
                    
                    let mail = {
                        to: "sommai.k@gmail.com",
                        subject: "welcome",
                        html: "<h1>welcome</h1>"
                    };
                    smtp.sendMail(mail, function(err, info){
                        smtp.close();
                        if(err){
                            console.log(err);
                            res.end("send email error.");
                        }else{
                            res.end("insert success");
                        }
                    });
                }
            });
        }
    });
    
});

router.put('/:id', function(req, res){
    let id = req.params.id;
    let user = req.body;
    let sql = `
        update user 
           set code = '${user.code}',
               name = '${user.name}'
         where id = ${id}
    `;
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    console.log(sql);
                    res.end("sql error");
                }else {
                    res.end("update success");
                }
            });
        }
    });
});

router.delete('/:id', function(req, res){
    let id = req.params.id;
    let sql = `
        delete from user where id = ${id}
    `
    
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    console.log(sql);
                    res.end("sql error");
                }else {
                    res.end("delete success");
                }
            });
        }
    });
});

router.get('/export', function(req, res) {
    let user = req.query;
    let sql = `
        select id, code, name
          from user
         where code like('%${user.code}%')
           and name like('%${user.name}%')
    `
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();
                if(error) {
                    res.end("sql error");
                }else {
                    let wb = new excel.Workbook();
                    let ws = wb.addWorksheet("Sheet 1");
                    ws.cell(1, 1).string("ID");
                    ws.cell(1, 2).string("Code");
                    ws.cell(1, 3).string("Name");
                    for(i = 0; i< result.length; i++) {
                        let user = result[i];
                        ws.cell(i+2, 1).number(user.id);
                        ws.cell(i+2, 2).string(user.code);
                        ws.cell(i+2, 3).string(user.name);
                    }
                    
                    res.header("Content-Type", "application/octet-stream");
                    wb.write("user.xlsx", res);
                }
            });
        }
    });
});

module.exports = router;
// https://gitlab.com/sommai.k/simple2