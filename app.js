
var express = require("express");
const app = express();
const server = require("http").Server(app);
const user = require("./user");
var bodyParser = require('body-parser');

app.use(bodyParser.json());

app.get('/', function(req, res) {
    res.end("yes you can");
});

app.get('/ab?cd', function(req, res){
    res.end("this is a abcd route");
});

app.get('/users/:userId/books/:bookId', function(req, res) {
    let userId = req.params.userId;
    let bookId = req.params.bookId;
    res.end('userId = '+userId);
});

app.use('/user', user);

server.listen(8000);
console.log("Server listen on port 8000");
// http.createServer( function(req, res){
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//     res.end("yes you can.");
// }).listen(8000);

// console.log("Server listen on port 8000");

// const server = http.createServer().listen(8000);
// const serverHandler = require("./server");

// server.on('request', serverHandler);

// console.log("Server listen on port 8000");