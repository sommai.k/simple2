FROM node:14-alpine
WORKDIR /app
COPY . /app
RUN npm i
EXPOSE 8000
CMD ["node", "app.js"]

# docker build -t first .
# docker build -t first -f Dockerfile88 .
# docker image ls
# docker run --name first -p 8000:8000 -d first
# docker-compose up -d
# docker tag first registry.gitlab.com/sommai.k/simple2
# docker build -t registry.gitlab.com/sommai.k/simple2 .
# docker push registry.gitlab.com/sommai.k/simple2